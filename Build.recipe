<Project DefaultTargets="All" xmlns="http://schemas.microsoft.com/developer/msbuild/2003">

  <PropertyGroup>
    <RecipeVersion>1.2</RecipeVersion>
    <ArtifactSolution>Repertoire</ArtifactSolution>
    <ArtifactAuthors>Thomas la Cour</ArtifactAuthors>
    <ArtifactCompany>Hyrtwol</ArtifactCompany>
    <ArtifactCopyright>Copyright 2013 $(ArtifactAuthors)</ArtifactCopyright>
    <ArtifactProjectUrl>http://hyrtwol.dk/repertoire/</ArtifactProjectUrl>
    <ArtifactIconUrl>http://hyrtwol.dk/icons/repertoire.ico</ArtifactIconUrl>
    <ArtifactLicenseUrl>http://hyrtwol.dk/repertoire/mit.txt</ArtifactLicenseUrl>
  </PropertyGroup>

  <!-- Publish -->
  <PropertyGroup>
    <PublishVersion>$(RecipeVersion).12</PublishVersion>
    <!-- local myget nugetorg -->
    <PublishTo>myget</PublishTo>
    <PublishWhatIf>false</PublishWhatIf>
  </PropertyGroup>

  <ItemGroup>
    <Project Include="Repertoire">
      <Flavour>NuGet</Flavour>
      <Description>A collection of utilities for creating small websites. Only depending on System.Web</Description>
      <Publish>true</Publish>
    </Project>
    <Project Include="Repertoire.Artwork">
      <Flavour>NuGet</Flavour>
      <Description>HttpHandlers for resizing images on the fly</Description>
      <Publish>true</Publish>
    </Project>
    <Project Include="Repertoire.jQuery">
      <Flavour>NuGet</Flavour>
      <Description>jQuery as Embedded Resource</Description>
      <Publish>true</Publish>
    </Project>
    <Project Include="Repertoire.TwitterBootstrap">
      <Flavour>NuGet</Flavour>
      <Description>Twitter Bootstrap as Embedded Resource</Description>
      <Publish>true</Publish>
    </Project>
    <Project Include="Repertoire.Markdown">
      <Flavour>NuGet</Flavour>
      <Description>HttpHandler for showing Markdown as html</Description>
      <Publish>true</Publish>
    </Project>
  </ItemGroup>

  <Import Project="$(Pantry)\Fry.targets" Condition=" Exists('$(Pantry)\Fry.targets') "/>

  <ItemGroup>
    <DeployGoal Condition=" '$(PublishTo)' != 'nugetorg' " Include="NuGetDeployPackagesDependencies" />
  </ItemGroup>

  <Target Name="PrepareXmlPokeProjects">
    <ItemGroup>
      <PokeProjects Include="**\*.csproj" Exclude="**\bin\**\*.csproj"/>
    </ItemGroup>
    <MSBuild Projects="$(MSBuildProjectFile)"
         Targets="XmlPokeProject"
         Properties="PokeProject=%(PokeProjects.Identity);PokeProjectName=%(PokeProjects.Filename);PokeTargetFrameworkVersion=v4.0"
         Condition=" '@(PokeProjects)' != '' " />
  </Target>

  <Target Name="XmlPokeProject">
    <Message Text="Poking '$(PokeProject)' name '$(PokeProjectName)'" Importance="high" />
    <Message Text="TargetFrameworkVersion -> '$(PokeTargetFrameworkVersion)'" Importance="high" />
    <XmlPoke XmlInputPath="$(PokeProject)"
             Query="/x:Project/x:PropertyGroup/x:TargetFrameworkVersion"
             Value="$(PokeTargetFrameworkVersion)"
             Namespaces="&lt;Namespace Prefix='x' Uri='http://schemas.microsoft.com/developer/msbuild/2003' /&gt;" />

    <Message Text="RootNamespace -> '$(PokeProjectName)'" Importance="high" />
    <XmlPoke XmlInputPath="$(PokeProject)"
             Query="/x:Project/x:PropertyGroup/x:RootNamespace"
             Value="$(PokeProjectName)"
             Namespaces="&lt;Namespace Prefix='x' Uri='http://schemas.microsoft.com/developer/msbuild/2003' /&gt;" />
    <Message Text="AssemblyName -> '$(PokeProjectName)'" Importance="high" />
    <XmlPoke XmlInputPath="$(PokeProject)"
             Query="/x:Project/x:PropertyGroup/x:AssemblyName"
             Value="$(PokeProjectName)"
             Namespaces="&lt;Namespace Prefix='x' Uri='http://schemas.microsoft.com/developer/msbuild/2003' /&gt;" />
  </Target>

  <!--
  <Target Name="NuGetDeployPackagesDependencies" Condition=" '$(PublishTo)' != 'nugetorg' ">
    <ItemGroup>
      <NupkgDependency Include="$(PackagesDir)\**\*.nupkg" />
    </ItemGroup>

    <Message Text="NupkgDependency=%(NupkgDependency.Identity)" Importance="high" />
    <Error Condition="!Exists('%(NupkgDependency.Identity)')" Text="Missing file %(NupkgDependency.Identity)" />

    <NuGetPush PackagePath="%(NupkgDependency.Identity)"
               Source="$(NuGetPublishUrl)"
               ApiKey="$(NuGetApiKey)"
               Verbosity="detailed" />
  </Target>
  -->

  <Target Name="Publish" DependsOnTargets="Clean">
    <PropertyGroup>
      <ArtifactsFolder Condition=" '$(ArtifactsFolder)' == '' ">$(MSBuildProjectDirectory)\artifacts</ArtifactsFolder>
      <!-- $(LocalPackageSource) $(KlondikeOntogenesisFeedUrl) -->
      <PublishSourceFeed>$(KlondikeOntogenesisFeedUrl)</PublishSourceFeed>
    </PropertyGroup>

    <Message Text="Publishing version $(PublishVersion) to '$(PublishTo)'" />
    <Message Text="Using source feed '$(PublishSourceFeed)'" />
    
    <Error Condition="'$(PublishTo)'==''" Text="PublishTo is not defined" />
    <Error Condition="'$(PublishSourceFeed)'==''" Text="PublishSourceFeed is not defined" />

    <RemoveDir Directories="$(ArtifactsFolder)" Condition=" Exists('$(ArtifactsFolder)') " />
    <MakeDir Directories="$(ArtifactsFolder)" />

    <NuGetInstall Source="$(PublishSourceFeed)"
                  Condition="%(Project.Publish)"
                  PackagesConfig="%(Project.Identity)"
                  OutputDirectory="$(ArtifactsFolder)"
                  Verbosity="detailed"
                  Version="$(PublishVersion)"
                  ExcludeVersion="false"
                  NoCache="true" />

    <ItemGroup>
      <Nupkg Include="$(ArtifactsFolder)\**\$(ArtifactSolution)*.nupkg" />
    </ItemGroup>

    <Message Text="Nupkg=%(Nupkg.Identity)" Importance="high" />
    <Error Condition="'@(Nupkg)'==''" Text="Nothing to publish searching '$(ArtifactsFolder)\**\$(ArtifactSolution)*.nupkg'" />
    <Error Condition="!Exists('%(Nupkg.Identity)')" Text="Missing file %(Nupkg.Identity)" />

    <PropertyGroup Condition=" '$(PublishTo)' == 'local' ">
      <_NuGetPublishUrl>$(NuGetPublishUrl)</_NuGetPublishUrl>
      <_NuGetApiKey>$(NuGetApiKey)</_NuGetApiKey>
    </PropertyGroup>
    <PropertyGroup Condition=" '$(PublishTo)' == 'myget' ">
      <_NuGetPublishUrl>$(MyGetPublishUrl)</_NuGetPublishUrl>
      <_NuGetApiKey>$(MyGetApiKey)</_NuGetApiKey>
    </PropertyGroup>
    <PropertyGroup Condition=" '$(PublishTo)' == 'nugetorg' ">
      <_NuGetPublishUrl>$(NuGetOrgPublishUrl)</_NuGetPublishUrl>
      <_NuGetApiKey>$(NuGetOrgApiKey)</_NuGetApiKey>
    </PropertyGroup>

    <Message Text="Pushing to '$(_NuGetPublishUrl)' with apikey '$(_NuGetApiKey)'" />
    
    <NuGetPush PackagePath="%(Nupkg.Identity)"
               Source="$(_NuGetPublishUrl)"
               ApiKey="$(_NuGetApiKey)"
               Verbosity="detailed"
               Condition="!$(PublishWhatIf)" />

    <Message Text="WhatIf: %(Nupkg.Identity)" Condition="$(PublishWhatIf)" />

  </Target>

</Project>