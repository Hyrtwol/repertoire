﻿using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Repertoire.Handlers
{
    public class ArtworkPng : ArtworkHttpHandler
    {
        protected override void SaveImage(Stream outputStream, Image image)
        {
            image.Save(outputStream, ImageFormat.Png);
        }

        protected override string ContentType { get { return "image/png"; } }
    }
}