using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using Repertoire.Artwork;

namespace Repertoire.Handlers
{
    public class ArtworkJpeg : ArtworkHttpHandler
    {
        private static readonly ImageCodecInfo ImageCodecInfo = ImageUtils.GetImageCodecInfo(ImageFormat.Jpeg);

        protected override void SaveImage(Stream outputStream, Image image)
        {
            var encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = new EncoderParameter(Encoder.Quality, ArtworkConfig.ArtworkJpegQuality);
            image.Save(outputStream, ImageCodecInfo, encoderParams);
        }

        protected override string ContentType { get { return "image/jpeg"; } }
    }
}