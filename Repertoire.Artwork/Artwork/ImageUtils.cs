using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Repertoire.Artwork
{
    public static class ImageUtils
    {
        public const string DefaultSourceImageExtension = ".jpg";

        public static ArtworkInfo ParseFileName(string fileName)
        {
            const string pattern =
                //"(?<uid>[0-9a-fA-F]+)-(w(?<width>[0-9]+)h(?<height>[0-9]+)|w(?<width>[0-9]+)|h(?<height>[0-9]+))(?<ext>\\.[0-9a-zA-Z]+)";
                "(?<uid>[0-9a-zA-Z_]+)-(w(?<width>[0-9]+)h(?<height>[0-9]+)|w(?<width>[0-9]+)|h(?<height>[0-9]+))(?<ext>\\.[0-9a-zA-Z]+)";

            var match = Regex.Match(fileName, pattern);
            if (!match.Success) return null;

            var uid = match.Groups["uid"];
            if (uid.Index > 0) return null;

            int width, height;
            if (!int.TryParse(match.Groups["width"].Value, out width)) width = 0;
            if (!int.TryParse(match.Groups["height"].Value, out height)) height = 0;

            var artworkInfo = new ArtworkInfo();
            artworkInfo.Uid = uid.Value.ToLower();
            artworkInfo.Size = new Size(width, height);

            artworkInfo.Extension = match.Groups["ext"].Value;

            return artworkInfo;
        }

        public static ImageCodecInfo GetImageCodecInfo(ImageFormat imageFormat)
        {
            return ImageCodecInfo.GetImageEncoders().FirstOrDefault(id => id.FormatID == imageFormat.Guid);
        }

        public static Image ScaleAndCrop(Image sourceImage, Size imageSize)
        {
            if (sourceImage == null) throw new ArgumentNullException("sourceImage");
            if (imageSize.Width < 1 && imageSize.Height < 1) throw new ArgumentOutOfRangeException("imageSize", "Either width or height must be bigger than zero.");

            if (imageSize.Width > sourceImage.Width) imageSize.Width = sourceImage.Width;
            if (imageSize.Height > sourceImage.Height) imageSize.Height = sourceImage.Height;

            if (sourceImage.Size == imageSize) return sourceImage;

            if (imageSize.Width < 1) imageSize.Width = imageSize.Height*sourceImage.Width/sourceImage.Height;
            if (imageSize.Height < 1) imageSize.Height = imageSize.Width*sourceImage.Height/sourceImage.Width;

            int destX = 0;
            int destY = 0;

            float nPercent;
            float nPercentW = (imageSize.Width/(float) sourceImage.Width);
            float nPercentH = (imageSize.Height/(float) sourceImage.Height);

            if (nPercentH < nPercentW)
            {
                nPercent = nPercentW;
                destY = (int) ((imageSize.Height - (sourceImage.Height*nPercent))/2);
            }
            else
            {
                nPercent = nPercentH;
                destX = (int) ((imageSize.Width - (sourceImage.Width*nPercent))/2);
            }

            var destWidth = (int) (sourceImage.Width*nPercent);
            var destHeight = (int) (sourceImage.Height*nPercent);

            var result = new Bitmap(imageSize.Width, imageSize.Height);
            result.SetResolution(sourceImage.HorizontalResolution, sourceImage.VerticalResolution);

            using (var graphics = Graphics.FromImage(result))
            {
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.SmoothingMode = SmoothingMode.AntiAlias;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                graphics.DrawImage(sourceImage, new Rectangle(destX, destY, destWidth, destHeight), new Rectangle(0, 0, sourceImage.Width, sourceImage.Height), GraphicsUnit.Pixel);
            }

            return result;
        }

        public static Image CreateDummyImage(string name, Size imageSize)
        {
            Image image = new Bitmap(imageSize.Width, imageSize.Height);
            using (Graphics g = Graphics.FromImage(image))
            {
                var brush = new SolidBrush(Color.FromArgb(255, 100, 100));
                var font = new Font("Arial", 12);
                g.DrawString(name, font, brush, 10.0f, 10.0f);
            }
            return image;
        }

        public static void Resize(Stream input, Stream output, int width, int height)
        {
            using (var image = Image.FromStream(input))
            using (var bmp = new Bitmap(width, height))
            using (var graphics = Graphics.FromImage(bmp))
            {
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                graphics.DrawImage(image, new Rectangle(0, 0, width, height));
                bmp.Save(output, ImageFormat.Png);
            }
        }
    }
}