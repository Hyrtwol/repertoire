﻿using System;
using System.IO;
using System.Web;

namespace Repertoire.Artwork
{
    public static class HtmlExtensions
    {
        public static ArtworkInfo ParseArtworkUrl(this HttpRequest request)
        {
            if (request == null) throw new ArgumentNullException("request");

            var appRelPath = request.AppRelativeCurrentExecutionFilePath;
            if (string.IsNullOrEmpty(appRelPath))
                throw new ArgumentException("request.AppRelativeCurrentExecutionFilePath is null", "request");

            var fileName = Path.GetFileName(appRelPath);
            var artworkInfo = ImageUtils.ParseFileName(fileName);
            //if (artworkInfo == null) throw new UnableToParseArtworkUrlException(fileName);
            return artworkInfo;
        }

        //public static AssemblyName GetAssemblyNameFromUrl(this HttpRequest request, ArtifactFileInfo.ArtifactPackaging artifactPackaging)
        //{
        //    AssemblyName assemblyName = null;
        //    var segments = request.Url.AbsolutePath.Split('/');
        //    if (segments.Length > 3)
        //    {
        //        var parts = segments.Reverse().Take(3).ToArray();
        //        if (parts[2] == "Artifacts")
        //        {
        //            var info = ArtifactFileInfo.GetArtifactFileInfo(parts[0]);
        //            if (info.Packaging != artifactPackaging)
        //                return null;
        //            assemblyName = new AssemblyName(parts[1]) { Version = info.Version };
        //        }
        //    }
        //    return assemblyName;
        //}

        //public static void WriteDocBegin(this HttpResponse response, string title)
        //{
        //    response.WriteDocBegin();
        //    response.WriteHeadBegin();
        //    response.WriteTitle(title);
        //    //response.WriteLine("<script type=\"text/javascript\" src=\"/includes/jquery.js\"></script>");
        //    //response.WriteLine("<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js\"></script>");
        //    response.WriteStyles();
        //    response.WriteHeadEnd();

        //    response.WriteBodyBegin();
        //}

        //public static void WriteHeander(this HttpResponse response, string title)
        //{
        //    response.WriteHeadBegin();
        //    response.WriteTitle(title);
        //    //response.WriteStyles();
        //    //response.WriteLine("<link href=\"css/bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\">");
        //    response.WriteLink("css/bootstrap.css");
        //    response.WriteLink("css/bootstrap-responsive.css");
        //    response.WriteLink("css/docs.css");
        //    response.WriteHeadEnd();
        //}

        //public static void WriteStyles(this HttpResponse response)
        //{
        //    response.WriteLine("<style type=\"text/css\">");
        //    response.WriteLine("body { font-family: Calibri, Arial, Helvetica, sans-serif; }");
        //    response.WriteLine(".art { width: 160px; }");
        //    //response.WriteLine(".pot { text-align: center; width: 100px; background-color: #CCFF99; }");
        //    response.WriteLine(".pot { text-align: center; width: 40px; }");
        //    response.WriteLine("</style>");
        //}
    }
}