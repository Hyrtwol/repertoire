﻿using System;

namespace Repertoire.Artwork
{
    public class UnableToParseArtworkUrlException : Exception
    {
        public UnableToParseArtworkUrlException(string url)
            : base("Unable to parse url: "+url)
        {
        }
    }
}