﻿using System;
using System.Configuration;

namespace Repertoire
{
    public static class ArtworkConfig
    {
        public static readonly TimeSpan ArtworkCacheTime;
        public static readonly int ArtworkJpegQuality;

        static ArtworkConfig()
        {
            var settings = ConfigurationManager.AppSettings;
            ArtworkCacheTime = settings.ToTimeSpan("ArtworkCacheTime", TimeSpan.FromMinutes(10.0));
            ArtworkJpegQuality = settings.ToInt32("ArtworkJpegQuality");
        }
    }
}