using System.Drawing;

namespace Repertoire
{
    public class ArtworkInfo
    {
        //public Guid Guid;
        public string Uid;
        public Size Size;
        public string Extension;

        public override string ToString()
        {
            return string.Format("{0} {{ Uid={1}, Size={2}, Extension={3} }}",
                GetType().Name, Uid, Size, Extension);
        }
    }
}