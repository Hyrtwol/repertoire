﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Web;
using Repertoire.Artwork;

namespace Repertoire
{
    // http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
    // http://www.dotnetperls.com/cache

    public abstract class ArtworkHttpHandler : IHttpHandler
    {
        public bool IsReusable { get { return true; } }

        public void ProcessRequest(HttpContext context)
        {
            var request = context.Request;
            HttpResponse response = context.Response;

            try
            {
                var artworkInfo = request.ParseArtworkUrl();
                //if (artworkInfo == null) throw new UnableToParseArtworkUrlException(request.AppRelativeCurrentExecutionFilePath);

                response.ContentType = ContentType;
                response.SetCacheHeaders(ArtworkConfig.ArtworkCacheTime);

                if (artworkInfo != null)
                {
                    var artworkPath = Path.GetDirectoryName(request.PhysicalPath);
                    var ext = Path.GetExtension(request.PhysicalPath);
                    var fileName = Path.ChangeExtension(artworkInfo.Uid, ext);
                    var fullFileName = artworkPath != null ? Path.Combine(artworkPath, fileName) : fileName;

                    using (var sourceImage = Image.FromFile(fullFileName))
                    {
                        using (var image = ImageUtils.ScaleAndCrop(sourceImage, artworkInfo.Size))
                        {
                            SaveImage(response.OutputStream, image);
                        }
                    }
                    Debug.Print("Served {0}", artworkInfo);
                }
                else
                {
                    response.TransmitFile(request.PhysicalPath);
                }
            }
            catch (Exception ex)
            {
                response.StatusCode = 500;
                //response.ContentType = "text/plain";
                response.WriteException(ex);
            }
        }

        protected abstract void SaveImage(Stream outputStream, Image image);
        
        protected abstract string ContentType { get; }
    }


    //// Try and prevent request from a different domain
    //if (request.Url != null && request.UrlReferrer != null)
    //{
    //    if (String.Compare(request.Url.Host, request.UrlReferrer.Host,
    //        true, CultureInfo.InvariantCulture) != 0)
    //    {
    //        response.Write("<html>\r\n");
    //        response.Write("<head><title>JPG HTTP Handler</title></head>\r\n");
    //        response.Write("<body>\r\n");
    //        response.Write("<h1>Access Denied</h1>\r\n");
    //        response.Write("</body>\r\n");
    //        response.Write("</html>");
    //        return;
    //    }
    //}

    // Otherwise transfer requested file
    //try
    //{
    //    response.ContentType = "application/jpg";
    //    response.WriteFile(request.PhysicalPath);
    //}
    //catch
    //{
    //    response.Write("<html>\r\n");
    //    response.Write("<head><title>JPG HTTP Handler</title></head>\r\n");
    //    response.Write("<body>\r\n");
    //    response.Write("<h1>Access Granted but file was not found</h1>\r\n");
    //    response.Write("</body>\r\n");
    //    response.Write("</html>");
    //}
}