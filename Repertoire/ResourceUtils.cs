﻿using System;
using System.IO;
using System.Reflection;

namespace Repertoire
{
    public static class ResourceUtils
    {
        public static void CopyResourceToStream<T>(Stream stream, string name)
        {
            CopyResourceToStream(stream, typeof(T).Assembly, name);
        }

        public static void CopyResourceToStream(Stream stream, Assembly asm, string name)
        {
            using (var s = asm.GetManifestResourceStream(name))
            {
                if (s != null) s.CopyTo(stream);
                else throw new InvalidOperationException("Unable to get resource " + name + " from " + asm.FullName);
            }
        }

        public static string ReadResourceAsString<T>(string name)
        {
            return ReadResourceAsString(typeof(T).Assembly, name);
        }

        public static string ReadResourceAsString(Assembly asm, string name)
        {
            using (var s = asm.GetManifestResourceStream(name))
            {
                if (s == null) return null;
                using (var reader = new StreamReader(s))
                {
                    return reader.ReadToEnd();
                }
            }
        }

        public static string[] GetResourceNames<T>()
        {
            var asm = typeof(T).Assembly;
            return asm.GetManifestResourceNames();
        }
    }
}