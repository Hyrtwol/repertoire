﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace Repertoire
{
    public abstract class WebServerWithHttpContext : WebServerBase
    {
        protected WebServerWithHttpContext(params Uri[] prefixes)
            : base(prefixes)
        {
        }

        protected sealed override void ProcessRequest(HttpListenerContext httpListenerContext)
        {
            var url = httpListenerContext.Request.Url;
            var pathTranslated = url.Segments.Last();
            var httpRequest = new HttpRequest(pathTranslated, url.AbsoluteUri, url.Query);

            using (var mem = new MemoryStream(10000))
            {
                using (var writer = new StreamWriter(mem))
                {
                    writer.AutoFlush = true;

                    var httpResponse = new HttpResponse(writer);
                    var httpContext = new HttpContext(httpRequest, httpResponse);

                    ProcessRequest(httpContext);

                    byte[] buffer = mem.GetBuffer();

                    httpListenerContext.Response.ContentLength64 = buffer.Length;
                    httpListenerContext.Response.OutputStream.Write(buffer, 0, buffer.Length);
                }
            }
        }

        protected abstract void ProcessRequest(HttpContext context);
    }
}