﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Repertoire
{
    public interface IResourceModule
    {
        //void Register(IPageResourceRegistry registry);
        //IEnumerable<PageResource> GetPageResources();
        void Load(IPageResourceRegistry registry);
    }

    public abstract class ResourceModule : IResourceModule
    {
        public abstract void Load(IPageResourceRegistry registry);
    }


    public interface IPageResourceRegistry
    {
        void Register(params PageResource[] items);
    }

    public class PageResources : IPageResourceRegistry
    {
        private static volatile PageResources _instance;
        private static readonly object SyncRoot = new Object();

        private PageResources() { }

        public static PageResources Instance
        {
            get 
            {
                if (_instance == null) 
                {
                    lock (SyncRoot) 
                    {
                        if (_instance == null)
                            _instance = new PageResources();
                    }
                }

                return _instance;
            }
        }

        public static void Register<T>() where T : IResourceModule, new()
        {
            var pageResourceModule = new T();
            pageResourceModule.Load(Instance);
            //Instance.Register(pageResourceModule.GetPageResources());
        }


        private readonly PageResourceCollection _pageResources = new PageResourceCollection();

        //public PageResourceManager()
        //{
        //}

        public PageResources(params PageResource[] items)
        {
            Register(items);
        }

        public void Register(params PageResource[] items)
        {
            foreach (var item in items)
            {
                _pageResources.Add(item);
            }
        }

        public void Register(IEnumerable<PageResource> items)
        {
            foreach (var item in items)
            {
                _pageResources.Add(item);
            }
        }

        //public void Register<T>() where T : IPageResourceModule, new()
        //{
        //    var pageResourceModule = new T();
        //    foreach (var item in pageResourceModule.GetPageResources())
        //    {
        //        _pageResources.Add(item);
        //    }
        //}

        public PageResource Acquire(string id)
        {
            return _pageResources.Contains(id) ? _pageResources[id] : null;
        }

        private class PageResourceCollection : KeyedCollection<string, PageResource>
        {
            protected override string GetKeyForItem(PageResource item)
            {
                return item.Id;
            }
        }
    }
}