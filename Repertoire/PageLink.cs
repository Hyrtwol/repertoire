﻿using System;
using System.Web;

namespace Repertoire
{
    public class PageResource
    {
        // e.g. "jquery-1.9.1.min.js"
        public readonly string Id;

        // Write the html needed to include the resource
        // e.g. <link href="/markdownpad-github-dark.css" rel="stylesheet" />
        public readonly Action<HttpResponse> WriteReference;

        // Write the binary content to the http response
        public readonly Action<HttpResponse> WriteContent;

        public PageResource(string id, Action<HttpResponse> writeReference, Action<HttpResponse> writeContent)
        {
            Id = id;
            WriteReference = writeReference;
            WriteContent = writeContent;
        }
    }
}
