﻿using System;
using System.Net;
using System.Threading;

namespace Repertoire
{
    public abstract class WebServerBase
    {
        private readonly HttpListener _listener = new HttpListener();

        protected WebServerBase(params Uri[] prefixes)
        {
            if (!HttpListener.IsSupported) throw new NotSupportedException("Needs Windows XP SP2, Server 2003 or later.");

            if (prefixes == null || prefixes.Length == 0) throw new ArgumentException("prefixes");

            foreach (var prefix in prefixes)
            {
                Log(string.Format("Adding listener prefix {0}", prefix.AbsoluteUri));
                _listener.Prefixes.Add(prefix.AbsoluteUri);
            }
        }

        public event Action<string> LogEvent;

        public void Start()
        {
            _listener.Start();

            ThreadPool.QueueUserWorkItem(o =>
            {
                Log("Webserver running...");
                try
                {
                    while (_listener.IsListening)
                    {
                        var context = _listener.GetContext();
                        ThreadPool.QueueUserWorkItem(obj =>
                        {
                            var httpListenerContext = (HttpListenerContext) obj;
                            try
                            {
                                ProcessRequest(httpListenerContext);
                            }
                            catch (Exception ex)
                            {
                                Log(ex.Message);
                            }
                            finally
                            {
                                //httpListenerContext.Response.OutputStream.Flush();
                                httpListenerContext.Response.OutputStream.Close();
                            }
                        }, context);
                    }
                }
                catch (Exception ex)
                {
                    Log(ex.Message);
                }
            });
        }

        public void Stop()
        {
            _listener.Stop();
            _listener.Close();
        }

        protected abstract void ProcessRequest(HttpListenerContext httpListenerContext);

        protected void Log(string message)
        {
            var handler = LogEvent;
            if (handler == null) return;
            handler(message);
        }
    }
}
