﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Repertoire
{
    public static partial class HtmlExtensions
    {
        public static void WriteDocBegin(this HttpResponse response)
        {
            response.WriteLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            response.WriteDocTypeTransitional();
            response.WriteHtmlBegin();
        }

        //[Obsolete("Use WriteDocBegin")]
        //public static void BeginDoc(this HttpResponse response)
        //{
        //    response.WriteDocType();
        //    response.WriteHtmlBegin();
        //}

        public static void WriteDocEnd(this HttpResponse response)
        {
            response.WriteBodyEnd();
            response.WriteHtmlEnd();
        }

        [Obsolete("Use WriteDocEnd")]
        public static void EndDoc(this HttpResponse response)
        {
            response.WriteBodyEnd();
            response.WriteHtmlEnd();
        }

        public static void WriteDocTypeStrict(this HttpResponse response)
        {
            response.WriteLine("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">");
        }

        public static void WriteDocTypeTransitional(this HttpResponse response)
        {
            response.WriteLine("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
        }

        public static void WriteHtmlBegin(this HttpResponse response)
        {
            //response.WriteLine("<html>");
            response.WriteLine("<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">");
        }

        [Obsolete("Use WriteHtmlBegin")]
        public static void BeginHtml(this HttpResponse response)
        {
            //response.Write("<html>");
            response.WriteLine("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
            //response.Write("<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">");
        }

        public static void WriteHtmlEnd(this HttpResponse response)
        {
            response.Write("</html>");
        }

        [Obsolete("Use WriteHtmlEnd")]
        public static void EndHtml(this HttpResponse response)
        {
            response.Write("</html>");
        }

        public static void WriteHeadBegin(this HttpResponse response)
        {
            response.WriteLine("<head>");
        }

        [Obsolete("Use WriteHeadBegin")]
        public static void BeginHead(this HttpResponse response)
        {
            response.WriteLine("<head>");
        }

        public static void WriteHeadEnd(this HttpResponse response)
        {
            response.WriteLine("</head>");
        }

        [Obsolete("Use WriteHeadEnd")]
        public static void EndHead(this HttpResponse response)
        {
            response.WriteLine("</head>");
        }

        public static void WriteBodyBegin(this HttpResponse response)
        {
            response.WriteLine("<body>");
        }

        [Obsolete("Use WriteBodyBegin")]
        public static void BeginBody(this HttpResponse response)
        {
            response.WriteLine("<body>");
        }

        public static void WriteBodyEnd(this HttpResponse response)
        {
            response.WriteLine("</body>");
        }

        [Obsolete("Use WriteBodyEnd")]
        public static void EndBody(this HttpResponse response)
        {
            response.WriteLine("</body>");
        }

        public static void WriteTitle(this HttpResponse response, string title)
        {
            response.WriteLine("<title>{0}</title>", title);
        }

        public static void WriteLink(this HttpResponse response, string href)
        {
            response.WriteLine("<link href=\"{0}\" rel=\"stylesheet\" />",
                               VirtualPathUtility.ToAbsolute(href));
        }

        public static void WriteJavaScriptReference(this HttpResponse response, string src)
        {
            response.WriteLine("<script type=\"text/javascript\" src=\"{0}\"></script>",
                               VirtualPathUtility.ToAbsolute(src));
        }

        public static void WriteScriptBegin(this HttpResponse response)
        {
            response.WriteLine("<script type=\"text/javascript\">");
        }

        public static void WriteScriptEnd(this HttpResponse response)
        {
            response.WriteLine("</script>");
        }

        public static void WriteJavaScript(this HttpResponse response, Func<IEnumerable<string>> lines)
        {
            response.WriteScriptBegin();
            foreach (var line in lines())
            {
                response.WriteLine(line);
            }
            response.WriteScriptEnd();
        }

        public static void WriteJavaScript(this HttpResponse response, params string[] lines)
        {
            //response.WriteJavaScript(() => lines);
            response.WriteScriptBegin();
            foreach (var line in lines)
            {
                response.WriteLine(line);
            }
            response.WriteScriptEnd();
        }

        [Obsolete("Use WriteCssStyle")]
        public static void WriteStyles(this HttpResponse response, string styles)
        {
            response.Write("<style type=\"text/css\">");
            response.Write(styles);
            response.WriteLine("</style>");
        }

        public static void WriteCssStyle(this HttpResponse response, params string[] lines)
        {
            response.Write("<style type=\"text/css\">");
            foreach (var line in lines)
            {
                response.WriteLine(line);
            }
            response.WriteLine("</style>");
        }


        public static void WriteFooterBegin(this HttpResponse response)
        {
            response.WriteLine("<footer>");
        }

        public static void WriteFooterEnd(this HttpResponse response)
        {
            response.WriteLine("</footer>");
        }

        public static void WriteDivBegin(this HttpResponse response, string classValue = null)
        {
            if (classValue != null) response.WriteLine("<div class=\"{0}\">", classValue);
            else response.WriteLine("<div>");
        }

        public static void WriteDivEnd(this HttpResponse response)
        {
            response.WriteLine("</div>");
        }

        public static void WriteDivContainer(this HttpResponse response)
        {
            response.WriteDivBegin("container");
        }

        public static void WriteDivRow(this HttpResponse response)
        {
            response.WriteDivBegin("row");
        }

        public static void WriteDivSpan1(this HttpResponse response)
        {
            response.WriteDivBegin("span1");
        }

        public static void WriteDivSpan2(this HttpResponse response)
        {
            response.WriteDivBegin("span2");
        }

        public static void WriteDivSpan3(this HttpResponse response)
        {
            response.WriteDivBegin("span3");
        }

        public static void WriteDivSpan4(this HttpResponse response)
        {
            response.WriteDivBegin("span4");
        }

        public static void WriteDivSpan6(this HttpResponse response)
        {
            response.WriteDivBegin("span6");
        }

        public static void WriteHR(this HttpResponse response)
        {
            response.WriteLine("<hr/>");
        }

        public static void WriteP(this HttpResponse response, string text)
        {
            response.Write("<p>{0}</p>", text);
        }

        public static void WritePre(this HttpResponse response, string text)
        {
            response.Write("<pre>{0}</pre>", text);
        }

        public static void WriteH1(this HttpResponse response, string text)
        {
            response.Write("<h1>{0}</h1>", text);
        }

        public static void WriteH2(this HttpResponse response, string text)
        {
            response.Write("<h2>{0}</h2>", text);
        }

        public static void WriteH3(this HttpResponse response, string text)
        {
            response.Write("<h3>{0}</h3>", text);
        }

        public static void WriteH4(this HttpResponse response, string text)
        {
            response.WriteLine("<h4>{0}</h4>", text);
        }

        public static void WriteH5(this HttpResponse response, string text)
        {
            response.WriteLine("<h5>{0}</h5>", text);
        }

        public static void WriteA(this HttpResponse response, string href, object obj)
        {
            response.Write("<a href=\"{0}\">{1}<a/>", href, obj);
        }

        public static void WriteA(this HttpResponse response, string name, string href, string target = null)
        {
            if (string.IsNullOrEmpty(target))
                response.Write("<a href=\"{0}\">{1}<a/>", href, name);
            else
                response.Write("<a href=\"{0}\" target=\"{1}\">{2}<a/>", href, target, name);
        }

        public static void WriteErrorPage(this HttpResponse response, string title, string message, string code = null)
        {
            var pageTitle = string.Format("ERROR - {0}", title);
            response.WriteDocTypeTransitional();
            response.WriteHtmlBegin();
            response.WriteHeadBegin();
            response.WriteTitle(pageTitle);
            response.WriteHeadEnd();
            response.WriteBodyBegin();
            response.WriteH1(pageTitle);
            response.WriteH4(message);
            if (!string.IsNullOrEmpty(code)) response.WritePre(code);
            response.WriteBodyEnd();
            response.WriteHtmlEnd();
        }

        public static void WriteAttribute(this HttpResponse response, string name, string value)
        {
            if(string.IsNullOrEmpty(value)) return;
            response.Write(" {0}=\"{1}\"", name, value);
        }
    }
}
