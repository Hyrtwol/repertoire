﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace Repertoire
{
    public class TinyWebServer : WebServerWithHttpContext
    {
        private readonly Action<HttpContext> _response;

        public TinyWebServer(Action<HttpContext> response, params Uri[] prefixes)
            : base(prefixes)
        {
            if (response == null) throw new ArgumentException("response");
            _response = response;
        }

        protected override void ProcessRequest(HttpContext context)
        {
            _response(context);
        }
    }
}
