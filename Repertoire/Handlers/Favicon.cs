﻿using System;
using System.Drawing;
using System.Reflection;
using System.Web;

namespace Repertoire.Handlers
{
    /// <summary>
    /// Small demo to show an icon...
    /// </summary>
    public class Favicon : IHttpHandler
    {
        public static readonly Assembly ThisAssembly = typeof(Favicon).Assembly;

        public bool IsReusable { get { return true; } }

        public void ProcessRequest(HttpContext context)
        {
            var icon = GetIcon(context);

            var res = context.Response
                             .SetContentType("image/x-icon")
                             .SetCacheHeaders(TimeSpan.FromMinutes(10.0));

            if (icon != null)
            {
                res.WriteIcon(icon);
            }
            else
            {
                res.WriteResource<Favicon>("Repertoire.Repertoire.ico");
            }
        }

        private static Icon GetIcon(HttpContext context)
        {
            var ai = context.ApplicationInstance;
            var t = ai.GetType();
            var bt = t.BaseType;
            if (bt == null) return null;

            var asm = bt.Assembly;
            var location = asm.Location;
            return Icon.ExtractAssociatedIcon(location);
        }
    }
}