﻿using System;
using System.Web;

namespace Repertoire.Handlers
{
    /// <summary>
    /// Small demo to show an icon...
    /// </summary>
    public class RepertoireIcon : IHttpHandler
    {
        public bool IsReusable { get { return true; } }

        public void ProcessRequest(HttpContext context)
        {
            context.Response
                   .SetContentTypeByExtension(".ico")
                   .SetCacheHeaders(TimeSpan.FromMinutes(10.0))
                   .WriteResource<RepertoireIcon>("Repertoire.Repertoire.ico");
        }
    }
}