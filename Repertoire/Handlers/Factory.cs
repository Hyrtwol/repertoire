﻿using System;
using System.Diagnostics;
using System.IO;
using System.Web;
using DropNet;

namespace Markdownfall.Handlers
{
    public class Factory : IHttpHandlerFactory
    {
        private static readonly DropboxUser User = Config.CreateDropboxUser();

        private readonly DropNetClient _client;
        //private readonly UserLogin _userLogin;

        public Factory()
        {
            Debug.Print("DropboxSecrets: '{0}' '{1}' '{2}' '{3}'",
                        DropboxApp.ApiKey, DropboxApp.AppSecret,
                        User.UserToken, User.UserSecret);

            _client = User.CreateClient();
        }

        public IHttpHandler GetHandler(HttpContext context, string requestType, string url, string pathTranslated)
        {
            var request = context.Request;
            if (string.IsNullOrEmpty(request.AppRelativeCurrentExecutionFilePath)) return null;
            var path = request.AppRelativeCurrentExecutionFilePath.ToLower();

            IHttpHandler handler;
            if (path == "~/")
            {
                handler = new Redirect("~/index.md");
            }
            else
            {
                var ext = Path.GetExtension(path);
                switch (ext)
                {
                    case ".css":
                    case ".js":
                    case ".gif":
                    case ".png":
                    case ".jpg":
                        handler = new Resources();
                        break;
                    case ".markdown":
                        handler = new Markdown();
                        break;
                    case ".md":
                        handler = new Markdown();
                        //handler = new Dropbox(_client);
                        break;
                    case ".ico":
                        if (path == "~/favicon.ico") handler = new Favicon();
                        else handler = new Resources();
                        break;
                    default:
                        handler = new Html();
                        break;
                }
            }
            Debug.Print("[{0}] {1} ({2}) '{3}'", requestType, url, handler, path);
            return handler;
        }

        public void ReleaseHandler(IHttpHandler handler)
        {
        }
    }


}
