﻿using System;
using System.Diagnostics;
using System.IO;
using System.Web;

namespace Repertoire.Handlers
{
    public class Resource : IHttpHandler
    {
        public bool IsReusable { get { return true; } }

        public void ProcessRequest(HttpContext context)
        {
            var request = context.Request;
            var fileName = Path.GetFileName(request.FilePath);
            
            var pageResource = PageResources.Instance.Acquire(fileName);

            if (pageResource != null)
            {
                Debug.Print("Acquired embedded resource '{0}' for '{1}'", pageResource, fileName);
                pageResource.WriteContent(context.Response);
            }
            else
            {
                Debug.Print("Serving resource file '{0}'", request.CurrentExecutionFilePath);
                context.Response
                       .SetResourceCacheHeaders()
                       .SetContentTypeByExtension(request.CurrentExecutionFilePathExtension)
                       .TransmitFile(request.PhysicalPath);
            }
        }
    }
}