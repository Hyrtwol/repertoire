﻿using System;
using System.Diagnostics;
using System.Web;

namespace Repertoire.Handlers
{
    public class Redirect : IHttpHandler
    {
        private readonly string _redirectUrl;

        public Redirect(string redirectUrl)
        {
            if (string.IsNullOrEmpty(redirectUrl)) throw new ArgumentNullException("redirectUrl");
            _redirectUrl = redirectUrl;
        }

        public Redirect()
            : this(RepertoireConfig.RedirectUrl)
        {
        }

        public void ProcessRequest(HttpContext context)
        {
            var path = context.Request.AppRelativeCurrentExecutionFilePath;
            var response = context.Response;
            if (path == "~/")
            {
                Debug.Print("Redirecting to {0}", _redirectUrl);
                response.Redirect(_redirectUrl);
            }
            else
            {
                response.StatusCode = 404;
                response.SuppressContent = true;
                context.ApplicationInstance.CompleteRequest();
            }
        }

        public bool IsReusable { get { return true; } }
    }
}