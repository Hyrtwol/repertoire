﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Repertoire
{
    public static partial class HtmlExtensions
    {
        public static void WriteUnorderedList(this HttpResponse response, IEnumerable<object> values)
        {
            response.WriteLine("<ul>");
            foreach (var value in values)
            {
                response.Write("<li>");
                response.Write(value);
                response.WriteLine("</li>");
            }
            response.WriteLine("</ul>");
        }

        //public static void WriteUnorderedList(this HttpResponse response, IEnumerable<string> values)
        //{
        //}

        public static void WriteUnorderedList(this HttpResponse response, params object[] values)
        {
            response.WriteUnorderedList((IEnumerable<object>)values);
        }

        public static void WriteUnorderedList(this HttpResponse response, params string[] values)
        {
            response.WriteUnorderedList((IEnumerable<string>)values);
        }

        public static void WriteUnorderedList(this HttpResponse response, IEnumerable<KeyValuePair<string, object>> keyValuePairs)
        {
            response.WriteUnorderedList(keyValuePairs.Select(kv => string.Format("{0}='{1}'", kv.Key, kv.Value)));
        }
    }
}
