﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Web;

namespace Repertoire
{
    public static class HttpExtensions
    {
        public static readonly Dictionary<string, string> MimeTypes = new Dictionary<string, string>
        {
            //{".js", "text/javascript"},
            //{".js", "application/x-javascript"},
            {".js", "application/javascript"},
            {".txt", "text/plain"},
            {".css", "text/css"},
            {".htm", "text/html"},
            {".jpg", "image/jpeg"},
            {".ico", "image/x-icon"},
            {".gif", "image/gif"},
            {".png", "image/png"},
            {".exe", "application/octet-stream"},
            {".application", "application/x-ms-application"},
            {".manifest", "application/x-ms-manifest"},
            {".deploy", "application/octet-stream"}
        };

        private static readonly string NewLine = Environment.NewLine;
        private static readonly char[] SplitChars = new[] {'/'};

        #region HttpRequest

        public static string[] AppRelParts(this HttpRequest request)
        {
            var appRel = request.AppRelativeCurrentExecutionFilePath;
            if (appRel == null) return null;
            return appRel.ToLower().Split(SplitChars, StringSplitOptions.RemoveEmptyEntries);
        }

        public static string GetConfigPath(this HttpContext context, string samplePath)
        {
            return samplePath.StartsWith("~/") ? context.Server.MapPath(samplePath) : Path.GetFullPath(samplePath);
        }

        #endregion

        #region HttpResponse

        public static void Write(this HttpResponse response, string format, params object[] args)
        {
            response.Write(string.Format(format, args));
        }

        public static void WriteLine(this HttpResponse response)
        {
            response.Write(NewLine);
        }

        public static void WriteLine(this HttpResponse response, string text)
        {
            response.Write(text);
            response.Write(NewLine);
        }

        public static void WriteLine(this HttpResponse response, string format, params object[] args)
        {
            response.Write(string.Format(format, args));
            response.Write(NewLine);
        }

        public static void WriteException(this HttpResponse response, Exception exception)
        {
            response.WriteLine("<h1>ERROR</h1>");
            response.WriteLine("<h3>{0}</h3>", exception.Message);
            response.WriteLine("<pre>{0}</pre>", exception.StackTrace);
        }

        public static HttpResponse SetContentType(this HttpResponse response, string contentType)
        {
            response.ContentType = contentType;
            return response;
        }

        public static HttpResponse SetContentTypeByExtension(this HttpResponse response, string ext)
        {
            response.ContentType = MimeTypes[ext];
            //Debug.Print("ContentType={0}", response.ContentType);
            return response;
        }

        public static HttpResponse SetCacheHeaders(this HttpResponse response, TimeSpan cacheTime)
        {
            //Debug.Print("cacheTime={0}", cacheTime);
            response.Cache.SetExpires(DateTime.Now.Add(cacheTime));
            response.Cache.SetMaxAge(cacheTime);
            response.Cache.SetCacheability(HttpCacheability.Public);
            // ASP.NET ignores cache invalidation headers and the page remains in the cache until it expires.
            response.Cache.SetValidUntilExpires(true);
            return response;
        }

        //public static void TransmitFile(this HttpResponse response, FileEntry fileEntry)
        //{
        //    //response.WriteFile(fileEntry.FullName);
        //    response.TransmitFile(fileEntry.FullName);
        //}

        public static void WriteResource<T>(this HttpResponse response, string name)
        {
            ResourceUtils.CopyResourceToStream<T>(response.OutputStream, name);
        }

        public static void WriteResource(this HttpResponse response, Assembly asm, string name)
        {
            ResourceUtils.CopyResourceToStream(response.OutputStream, asm, name);
        }

        public static void WriteIcon(this HttpResponse response, Icon icon)
        {
            icon.Save(response.OutputStream);
        }

        #endregion
    }
}
