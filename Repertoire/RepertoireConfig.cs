﻿using System;
using System.Configuration;
using System.Web;

namespace Repertoire
{
    public static class RepertoireConfig
    {
        public static readonly TimeSpan ResourceCacheTime;
        public static readonly string RedirectUrl;

        static RepertoireConfig()
        {
            var settings = ConfigurationManager.AppSettings;
            ResourceCacheTime = settings.ToTimeSpan("RepertoireResourceCacheTime", TimeSpan.FromMinutes(10.0));
            RedirectUrl = settings.ToString("RepertoireRedirectUrl", "~/index.md");
        }

        public static HttpResponse SetResourceCacheHeaders(this HttpResponse response)
        {
            return response.SetCacheHeaders(ResourceCacheTime);
        }

    }
}