﻿using System;
using System.Collections.Specialized;

namespace Repertoire
{
    public static class NameValueCollectionExtensions
    {
        public static bool ToBool(this NameValueCollection nameValueCollection, string name, bool defaultValue = false)
        {
            var value = GetValue(nameValueCollection, name);
            if (string.IsNullOrWhiteSpace(value)) return defaultValue;
            bool result;
            return bool.TryParse(value, out result) ? result : defaultValue;
        }

        public static string ToString(this NameValueCollection nameValueCollection, string name, string defaultValue = null)
        {
            var value = GetValue(nameValueCollection, name);
            if (string.IsNullOrWhiteSpace(value)) return defaultValue;
            return string.IsNullOrWhiteSpace(value) ? null : value;
        }

        public static string GetValue(NameValueCollection nameValueCollection, string name)
        {
            if (nameValueCollection == null) throw new ArgumentNullException("nameValueCollection");
            if (string.IsNullOrEmpty(name)) throw new ArgumentNullException("name");
            return nameValueCollection[name];
        }

        public static TimeSpan ToTimeSpan(this NameValueCollection nameValueCollection, string name)
        {
            var value = GetValue(nameValueCollection, name);
            TimeSpan result;
            return TimeSpan.TryParse(value, out result) ? result : TimeSpan.Zero;
        }

        public static TimeSpan ToTimeSpan(this NameValueCollection nameValueCollection, string name, TimeSpan defaultValue)
        {
            var value = GetValue(nameValueCollection, name);
            TimeSpan result;
            return TimeSpan.TryParse(value, out result) ? result : defaultValue;
        }

        public static int ToInt32(this NameValueCollection nameValueCollection, string name, int defaultValue = 0)
        {
            var value = GetValue(nameValueCollection, name);
            int result;
            return int.TryParse(value, out result) ? result : defaultValue;
        }
    }
}