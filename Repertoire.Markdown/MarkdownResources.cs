﻿using System;
using System.Reflection;

namespace Repertoire
{
    public class MarkdownResources : IResourceModule
    {
        public static readonly Assembly ThisAssembly = typeof(MarkdownResources).Assembly;

        public void Load(IPageResourceRegistry registry)
        {
            // mdd_styles.css
            // mdd_toolbar.png
            // mdd_modal_background.png
            // mdd_gripper.png
            // MarkdownDeepLib.min.js
            // mdd_help.htm
            // mdd_ajax_loader.gif

            // Markdown.ico
            // markdownpad-github.css
            // jquery-1.9.1.min.js

            registry.Register(

                new PageResource(
                    "markdown.ico",
                    r => r.WriteJavaScriptReference("~/markdown.ico"),
                    r => r.SetContentTypeByExtension(".ico")
                          .SetResourceCacheHeaders()
                          .WriteResource(ThisAssembly, "Repertoire.Markdown.ico")),

                new PageResource(
                    "MarkdownDeepLib.min.js",
                    r => r.WriteJavaScriptReference("~/scripts/MarkdownDeepLib.min.js"),
                    r => r.SetContentTypeByExtension(".js")
                          .SetResourceCacheHeaders()
                          .WriteResource(ThisAssembly, "Repertoire.Scripts.MarkdownDeepLib.min.js")),

                new PageResource(
                    "mdd_ajax_loader.gif",
                    r => r.WriteJavaScriptReference("~/scripts/mdd_ajax_loader.gif"),
                    r => r.SetContentTypeByExtension(".gif")
                          .SetResourceCacheHeaders()
                          .WriteResource(ThisAssembly, "Repertoire.Scripts.mdd_ajax_loader.gif")),

                new PageResource(
                    "mdd_gripper.png",
                    r => r.WriteJavaScriptReference("~/scripts/mdd_gripper.png"),
                    r => r.SetContentTypeByExtension(".png")
                          .SetResourceCacheHeaders()
                          .WriteResource(ThisAssembly, "Repertoire.Scripts.mdd_gripper.png")),

                new PageResource(
                    "mdd_help.htm",
                    r => r.WriteJavaScriptReference("~/scripts/mdd_help.htm"),
                    r => r.SetContentTypeByExtension(".htm")
                          .SetResourceCacheHeaders()
                          .WriteResource(ThisAssembly, "Repertoire.Scripts.mdd_help.htm")),

                new PageResource(
                    "mdd_modal_background.png",
                    r => r.WriteJavaScriptReference("~/scripts/mdd_modal_background.png"),
                    r => r.SetContentTypeByExtension(".png")
                          .SetResourceCacheHeaders()
                          .WriteResource(ThisAssembly, "Repertoire.Scripts.mdd_modal_background.png")),

                new PageResource(
                    "mdd_styles.css",
                    r => r.WriteJavaScriptReference("~/scripts/mdd_styles.css"),
                    r => r.SetContentTypeByExtension(".css")
                          .SetResourceCacheHeaders()
                          .WriteResource(ThisAssembly, "Repertoire.Scripts.mdd_styles.css")),

                new PageResource(
                    "mdd_toolbar.png",
                    r => r.WriteJavaScriptReference("~/scripts/mdd_toolbar.png"),
                    r => r.SetContentTypeByExtension(".png")
                          .SetResourceCacheHeaders()
                          .WriteResource(ThisAssembly, "Repertoire.Scripts.mdd_toolbar.png")),

                new PageResource(
                    "markdownpad-github.css",
                    r => r.WriteJavaScriptReference("~/scripts/markdownpad-github.css"),
                    r => r.SetContentTypeByExtension(".css")
                          .SetResourceCacheHeaders()
                          .WriteResource(ThisAssembly, "Repertoire.Scripts.markdownpad-github.css")),

                new PageResource(
                    "markdownpad-github-dark.css",
                    r => r.WriteJavaScriptReference("~/scripts/markdownpad-github-dark.css"),
                    r => r.SetContentTypeByExtension(".css")
                          .SetResourceCacheHeaders()
                          .WriteResource(ThisAssembly, "Repertoire.Scripts.markdownpad-github-dark.css"))

                );
        }
    }
}
