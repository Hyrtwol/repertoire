﻿using System;
using System.Diagnostics;
using System.IO;
using System.Web;
using Repertoire.Markdown;

namespace Repertoire
{
    public class MarkdownWebServer : WebServerWithHttpContext
    {
        private const string EmbeddedCss = "~/scripts/markdownpad-github.css";
        private const string CssEdit = "~/scripts/mdd_styles.css";
        private const string JsJQuery = "~/scripts/jquery.js";
        private const string JsMarkdownDeepLib = "~/scripts/MarkdownDeepLib.min.js";

        public MarkdownWebServer(params Uri[] prefixes)
            : base(prefixes)
        {
        }

        protected override void ProcessRequest(HttpContext context)
        {
            Debug.Print("path={0}", context.Request.Path);
            var query = context.Request.QueryString.ToString().ToLower();
            var source = (query == "source");
            var edit = (query == "edit");

            var response = context.Response;

            if (source)
            {
                //response.ContentType = contentType;
                var filename = GetPhysicalPath(context);
                response.SetContentTypeByExtension(".txt").TransmitFile(filename);
                return;
            }

            response.SetCacheHeaders(MarkdownConfig.MarkdownCacheTime);

            response.WriteDocBegin();

            response.WriteHeadBegin();
            response.WriteTitle(GetTitle(context));
            WriteCssStyles(context);
            if (edit) response.WriteLink(CssEdit);
            response.WriteHeadEnd();

            response.WriteBodyBegin();
            try
            {
                //WriteHeader(context);
                var markdownText = GetMarkdownText(context);

                if (edit)
                {
                    WriteEditor(response, markdownText);
                }
                else
                {
                    response.WriteMarkdownAsHtml(markdownText);
                }
                //WriteFooter(context);

                if (edit) WriteJavaScriptForEditor(response);
            }
            catch (Exception ex)
            {
                response.WriteH1("ERROR");
                response.WriteH3(ex.Message);
                response.WritePre(ex.StackTrace);
            }
            response.WriteDocEnd();
        }

        protected virtual string GetTitle(HttpContext context)
        {
            return Path.GetFileNameWithoutExtension(context.Request.Path);
        }

        protected virtual void WriteCssStyles(HttpContext context)
        {
            /*
            var css = MarkdownConfig.MarkdownCssFile;
            if (string.IsNullOrEmpty(css)) css = EmbeddedCss;

            var embed = MarkdownConfig.MarkdownCssEmbed;
            if (embed)
            {
                var cssFile = context.Server.MapPath(css);
                var cssText = File.ReadAllText(cssFile);
                context.Response.WriteCssStyle(cssText);
            }
            else
            {
                context.Response.WriteLink(css);
            }
            */
            var cssText = ResourceUtils.ReadResourceAsString<MarkdownResources>("Repertoire.Scripts.markdownpad-github.css");
            context.Response.WriteCssStyle(cssText);
        }

        protected virtual string GetMarkdownText(HttpContext context)
        {
            var path = GetPhysicalPath(context);
            var markdownText = File.ReadAllText(path);
            return markdownText;
        }

        protected virtual string GetPhysicalPath(HttpContext context)
        {
            var path = string.Join("\\", context.Request.Path.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries));
            var physicalPath = Path.GetFullPath(path);
            //Debug.Print("physicalPath={0}", physicalPath);
            return physicalPath;
        }

        protected virtual void WriteEditor(HttpResponse response, string markdownText)
        {
            response.WriteLine("<div class=\"mdd_toolbar\"></div>");
            response.Write("<textarea cols=20 rows=20 class=\"mdd_editor\">");
            response.Write(markdownText);
            response.WriteLine("</textarea>");
            response.WriteLine("<div class=\"mdd_resizer\"></div>");
            response.WriteLine("<div class=\"mdd_preview\"></div>");
        }

        protected virtual void WriteJavaScriptForEditor(HttpResponse response)
        {
            response.WriteJavaScriptReference(JsJQuery);
            response.WriteJavaScriptReference(JsMarkdownDeepLib);
            response.WriteJavaScript("$(function () {",
                                     "	$(\"textarea.mdd_editor\").MarkdownDeep({",
                                     "		help_location:\"/Scripts/mdd_help.htm\",",
                                     "		ExtraMode: true",
                                     "	});",
                                     "})");
        }
    }
}
