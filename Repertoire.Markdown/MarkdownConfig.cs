﻿using System;
using System.Configuration;

namespace Repertoire
{
    public static class MarkdownConfig
    {
        /// <summary>
        /// If this file exists it will be used insted of the default
        /// </summary>
        public static readonly string MarkdownCssFile;

        /// <summary>
        /// If true the css will be inlined otherwise linked
        /// </summary>
        public static readonly bool MarkdownCssEmbed;

        public static readonly string MarkdownRootFolder;

        public static readonly TimeSpan MarkdownCacheTime;

        static MarkdownConfig()
        {
            var settings = ConfigurationManager.AppSettings;
            MarkdownCssFile = settings.ToString("MarkdownCssFile");
            MarkdownCssEmbed = settings.ToBool("MarkdownCssEmbed");
            MarkdownCacheTime = settings.ToTimeSpan("MarkdownCacheTime", TimeSpan.FromMinutes(10.0));
            MarkdownRootFolder = settings.ToString("MarkdownRootFolder");
        }
    }
}