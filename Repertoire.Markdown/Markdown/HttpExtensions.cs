﻿using System;
using System.Web;

namespace Repertoire.Markdown
{
    public static class HttpExtensions
    {
        public static void WriteMarkdownAsHtml(this HttpResponse response, string markdownText)
        {
            var markdown = new MarkdownDeep.Markdown();
            // Set options
            markdown.ExtraMode = true;
            markdown.SafeMode = false;
            var html = markdown.Transform(markdownText);
            response.Write(html);
        }
    }
}
