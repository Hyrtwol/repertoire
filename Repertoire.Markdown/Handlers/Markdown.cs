﻿using System;
using System.IO;
using System.Text;
using System.Web;

namespace Repertoire.Handlers
{
    // http://www.toptensoftware.com/markdowndeep/editor

    public class Markdown : IHttpHandler
    {
        private const string EmbeddedCss = "~/scripts/markdownpad-github.css";
        private const string CssEdit = "~/scripts/mdd_styles.css";
        private const string JsJQuery = "~/scripts/jquery.js";
        private const string JsMarkdownDeepLib = "~/scripts/MarkdownDeepLib.min.js";

        private MarkdownDeep.Markdown _markdown;

        public virtual bool IsReusable { get { return true; } }

        public void ProcessRequest(HttpContext context)
        {
            var query = context.Request.QueryString.ToString().ToLower();
            var source = (query == "source");
            var edit = (query == "edit");

            var response = context.Response;

            if (source)
            {
                //response.ContentType = contentType;
                var filename = GetPhysicalPath(context);
                response.SetContentTypeByExtension(".txt").TransmitFile(filename);
                return;
            }

            response.SetCacheHeaders(MarkdownConfig.MarkdownCacheTime);

            response.WriteDocBegin();

            response.WriteHeadBegin();
            response.WriteTitle(GetTitle(context));
            WriteCssStyles(context);
            if (edit) response.WriteLink(CssEdit);
            response.WriteHeadEnd();

            response.WriteBodyBegin();
            try
            {
                WriteHeader(context);
                var markdownText = GetMarkdownText(context);

                if (edit)
                {
                    WriteEditor(response, markdownText);
                }
                else
                {
                    if (_markdown == null)
                    {
                        _markdown = new MarkdownDeep.Markdown
                        {
                            // Set options
                            ExtraMode = true,
                            SafeMode = false
                        };
                        _markdown.FormatCodeBlock += DoFormatCodeBlock;
                    }
                    var html = _markdown.Transform(markdownText);
                    response.Write(html);
                    // response.WriteMarkdownAsHtml(markdownText);
                }
                WriteFooter(context);

                if (edit) WriteJavaScriptForEditor(response);
            }
            catch (Exception ex)
            {
                response.WriteH1("ERROR");
                response.WriteH3(ex.Message);
                response.WritePre(ex.StackTrace);
            }
            response.WriteDocEnd();
        }

        private string DoFormatCodeBlock(MarkdownDeep.Markdown arg1, string arg2)
        {
            if (arg2.StartsWith("$$"))
            {
                if (arg2.StartsWith("$$toc$$"))
                {
                    var path2map = arg2.Substring(7).Trim();
                    var path = HttpContext.Current.Server.MapPath(path2map);
                    var prefix = path2map.Replace("~/", "/"); // $$toc$$~/sites/

                    var di = new DirectoryInfo(path);

                    var sb = new StringBuilder();
                    if (di.Exists)
                    {
                        sb.Append("<ul>");
                        //foreach (var item in Directory.GetDirectories(path, "*", SearchOption.TopDirectoryOnly))
                        foreach (var item in di.GetDirectories("*", SearchOption.TopDirectoryOnly))
                        {
                            sb.AppendLine("<li><a href=\"" + prefix + item.Name + "/\">" + item.Name + "</a></li>");
                        }
                        sb.AppendLine("</ul>");
                    }
                    else
                    {
                        sb.Append("<pre>Unable to locate " + di.FullName + "</pre>");
                    }
                    return sb.ToString();
                }
            }
            return "<pre><code>" + arg2 + "</code></pre>";
        }

        protected virtual void WriteHeader(HttpContext context)
        {
        }

        protected virtual void WriteFooter(HttpContext context)
        {
        }

        protected virtual string GetMarkdownText(HttpContext context)
        {
            var path = GetPhysicalPath(context);
            var markdownText = File.ReadAllText(path);
            return markdownText;
        }

        protected virtual void WriteCssStyles(HttpContext context)
        {
            var css = MarkdownConfig.MarkdownCssFile;
            if (string.IsNullOrEmpty(css)) css = EmbeddedCss;

            var embed = MarkdownConfig.MarkdownCssEmbed;
            if (embed)
            {
                var cssFile = context.Server.MapPath(css);
                var cssText = File.ReadAllText(cssFile);
                context.Response.WriteCssStyle(cssText);
            }
            else
            {
                context.Response.WriteLink(css);
            }
        }

        protected virtual string GetTitle(HttpContext context)
        {
            return Path.GetFileNameWithoutExtension(context.Request.Path);
        }

        protected virtual string GetPhysicalPath(HttpContext context)
        {
            return string.IsNullOrEmpty(MarkdownConfig.MarkdownRootFolder)
                       ? context.Request.PhysicalPath
                       : GetPhysicalPath(context, MarkdownConfig.MarkdownRootFolder);
        }

        protected static string GetPhysicalPath(HttpContext context, string root)
        {
            return context.Server.MapPath(root + context.Request.CurrentExecutionFilePath);
        }

        protected virtual void WriteEditor(HttpResponse response, string markdownText)
        {
            response.WriteLine("<div class=\"mdd_toolbar\"></div>");
            response.Write("<textarea cols=20 rows=20 class=\"mdd_editor\">");
            response.Write(markdownText);
            response.WriteLine("</textarea>");
            response.WriteLine("<div class=\"mdd_resizer\"></div>");
            response.WriteLine("<div class=\"mdd_preview\"></div>");
        }

        protected virtual void WriteJavaScriptForEditor(HttpResponse response)
        {
            response.WriteJavaScriptReference(JsJQuery);
            response.WriteJavaScriptReference(JsMarkdownDeepLib);
            response.WriteJavaScript("$(function () {",
                                     "	$(\"textarea.mdd_editor\").MarkdownDeep({",
                                     "		help_location:\"/Scripts/mdd_help.htm\",",
                                     "		ExtraMode: true",
                                     "	});",
                                     "})");
        }
    }
}
