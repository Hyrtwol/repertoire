﻿using System;
using System.IO;
using System.Web;

namespace Repertoire.Handlers
{
    public class MarkdownResources : IHttpHandler
    {
        public bool IsReusable { get { return true; } }

        public void ProcessRequest(HttpContext context)
        {
            var request = context.Request;
            var fileName = Path.GetFileName(request.FilePath);
            var ext = Path.GetExtension(fileName);
            var resourceName = "Repertoire.Scripts." + fileName;

            context.Response
                   .SetResourceCacheHeaders()
                   .SetContentTypeByExtension(ext)
                   .WriteResource<MarkdownResources>(resourceName);
        }
    }
}