﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Repertoire.WebTest.Handlers
{
    internal class TestHtmlHandler : IHttpHandler
    {
        private const string Title = "Repertoire.WebTest";

        public bool IsReusable { get { return true; } }

        public void ProcessRequest(HttpContext context)
        {
            //var request = context.Request;
            var response = context.Response;

            response.WriteDocBegin();
            response.WriteHeadBegin();
            response.WriteTitle(Title);
            response.WriteCssStyle("body { font-family: Calibri, Arial, Helvetica, sans-serif; }");
            response.WriteHeadEnd();
            response.WriteBodyBegin();

            WriteHeader(response);

            response.Write("<div id=\"content\">");
            try
            {
                WriteLinks(response, new[]
                {
                    "/index.md",
                    "/index.md?edit",
                    "/index.md?source",
                    "/sites/index.md",
                    "/sites/index.md?edit",
                    "/sites/index.md?source",
                    "/favicon.ico",
                    "/img/avatar.png",
                    "/img/test.jpg",
                    "/img/wall.jpg",
                    "/files/hello.zip",
                    "/markdown-dark.css",

                    "/markdown.ico",
                    "/scripts/markdownpad-github.css",
                    "/scripts/markdownpad-github-dark.css",

                    "/scripts/jquery.js",
                    "/scripts/mdd_help.htm"
                });
            }
            catch (Exception ex)
            {
                response.WriteH1("ERROR");
                response.WriteH3(ex.Message);
                response.WritePre(ex.StackTrace);
            }

            //response.WriteLine("<p>");
            //response.WriteLine(string.Format("test={0}", context.Items["test"]));
            //response.WriteLine("</p>");

            var appSettings = new Dictionary<string, object>
            {
                {"ArtworkCacheTime", ArtworkConfig.ArtworkCacheTime},
                {"ArtworkJpegQuality", ArtworkConfig.ArtworkJpegQuality},

                {"MarkdownCssFile", MarkdownConfig.MarkdownCssFile},
                {"MarkdownCssEmbed", MarkdownConfig.MarkdownCssEmbed}

                //{"DropboxEnabled", DropboxConfig.DropboxEnabled},
                //{"DropboxPullInterval", DropboxConfig.DropboxPullInterval},
                //{"DropboxFileExtensions", string.Join(";",  DropboxConfig.DropboxFileExtensions)},
                //{"DropboxApiKey", DropboxConfig.DropboxApiKey},
                //{"DropboxAppSecret", DropboxConfig.DropboxAppSecret},
                //{"DropboxUserToken", DropboxConfig.DropboxUserToken},
                //{"DropboxUserSecret", DropboxConfig.DropboxUserSecret}
            };

            response.WriteH3("CurrentDomain:");
            string dataDirectory = AppDomain.CurrentDomain.GetData("DataDirectory").ToString();
            response.WriteP(string.Format("DataDirectory={0}", dataDirectory));

            response.WriteH3("AppSettings:");
            response.WriteUnorderedList(appSettings);

            response.WriteH3("ConnectionStrings:");
            response.WriteUnorderedList(ConfigurationManager.ConnectionStrings.Cast<object>());

            response.WriteH3("jQuery.Resources:");
            response.WriteUnorderedList(jQueryResources.ThisAssembly.GetManifestResourceNames());

            response.WriteH3("Markdown.Resources:");
            response.WriteUnorderedList(MarkdownResources.ThisAssembly.GetManifestResourceNames());

            response.WriteH3("TwitterBootstrap.Resources:");
            response.WriteUnorderedList(TwitterBootstrapResources.ThisAssembly.GetManifestResourceNames());

            response.WriteLine("</div>");

            WriteFooter(response);

            response.WriteDocEnd();
        }

        private static void WriteHeader(HttpResponse response)
        {
            response.Write("<div id=\"header\">");
            response.WriteH1(Title);
            response.WriteHR();
            response.WriteLine("</div>");
        }

        private static void WriteFooter(HttpResponse response)
        {
            response.Write("<div id=\"footer\">");
            response.WriteHR();
            response.Write("<em>{0}</em>", typeof(TestHtmlHandler).Assembly.FullName);
            response.WriteLine("</div>");
        }

        private static void WriteLinks(HttpResponse response, IEnumerable<string> files)
        {
            response.WriteLine("<table cellpadding=\"4\">");
            response.WriteLine("<tr><th>Links</th></tr>");
            foreach (var uid in files)
            {
                response.Write("<tr>");
                response.Write("<td><a href=\"{0}\">{0}</a></td>", uid);
                response.WriteLine("</tr>");
            }
            response.WriteLine("</table>");
        }
    }
}
