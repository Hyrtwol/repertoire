![Hyrtwol](http://hyrtwol.dk/img/logo.png)

## Site

### Pages

* [Home](index.md)
* [Code](code/index.md)
* [Repertoire](repertoire/index.md)

## External

* [NuGet.org](https://www.nuget.org/profiles/Hyrtwol/)
* [NuGet source](http://nuget.codeplex.com/)
* [MyGet](https://www.myget.org/feed/Packages/cuisine/)
* [Bitbucket](https://bitbucket.org/)
* [GitHub](https://github.com/)
* [Dropbox](https://www.dropbox.com/) ([Apps](https://www.dropbox.com/developers/apps))
* [SharpDX](http://sharpdx.org/)
* [Bootstrap](http://getbootstrap.com/)
* [SS64 Command line reference – Web, Database and OS scripting.](http://ss64.com/)
* [Sick and Wrong](http://www.sickandwrongpodcast.com/)
* [Yogscast](http://www.yogscast.com/)
* [Digitally Imported](http://www.di.fm/)
* [2048 the game](http://gabrielecirulli.github.io/2048/)

## Test

1

    block 1
       hmmmmm

2

```
block 2
  sub block
```

3

    $$toc$$~/sites/

4


