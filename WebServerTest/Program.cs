﻿using System;
using System.Diagnostics;
using System.Net;
using System.Text;
using System.Web;
using Repertoire;

namespace WebServerTest
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var uri = new Uri("http://localhost:8080/");

            //var webServer = new TinyWebServer(ProcessRequest, uri);
            var webServer = new MarkdownWebServer(uri);
            webServer.LogEvent += Console.WriteLine;
            webServer.Start();
#if DEBUG
            Process.Start(uri.ToString() + "index.md");
#endif
            Console.WriteLine("A simple webserver. Press a key to quit.");
            Console.ReadKey();
            webServer.Stop();
        }

        private static void ProcessRequest(HttpContext context)
        {
            var request = context.Request;
            var response = context.Response;

            response.WriteDocBegin();
            response.WriteHeadBegin();
            response.WriteTitle("PageTitle");
            response.WriteCssStyle("body { font-family: Calibri, Arial, Helvetica, sans-serif; }");
            response.WriteHeadEnd();
            response.WriteBodyBegin();

            response.WriteH1("Hello");
            response.WriteP("Path: " + request.Path);

            response.WriteDocEnd();
        }

        private static void SendResponse(HttpListenerContext context, StringBuilder sb)
        {
            sb.AppendLine("<html><body>");
            sb.AppendFormat("My web page.<br>{0}", DateTime.Now);
            sb.AppendLine("</body></html>");
        }
    }
}
