﻿using System;
using System.Net;
using System.Text;
using Repertoire;

namespace WebServerTest
{
    public class TestWebServer1 : WebServerBase
    {
        private readonly Action<HttpListenerContext, StringBuilder> _response;

        public TestWebServer1(Action<HttpListenerContext, StringBuilder> response, params Uri[] prefixes)
            : base(prefixes)
        {
            if (response == null) throw new ArgumentException("response");
            _response = response;
        }

        protected override void ProcessRequest(HttpListenerContext httpListenerContext)
        {
            var sb = new StringBuilder(1024);
            _response(httpListenerContext, sb);
            var buf = Encoding.UTF8.GetBytes(sb.ToString());

            httpListenerContext.Response.ContentLength64 = buf.Length;
            httpListenerContext.Response.OutputStream.Write(buf, 0, buf.Length);
        }
    }
}
