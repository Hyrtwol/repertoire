﻿using System;
using System.Reflection;

namespace Repertoire
{
    public class jQueryResources : IResourceModule
    {
        public static readonly Assembly ThisAssembly = typeof(jQueryResources).Assembly;

        public void Load(IPageResourceRegistry registry)
        {
            registry.Register(
                new PageResource(
                    "jquery.js",
                    r => r.WriteJavaScriptReference("~/scripts/jquery-jquery-2.1.1.min.js"),
                    r => r.SetContentTypeByExtension(".js")
                          .SetResourceCacheHeaders()
                          .WriteResource(ThisAssembly, "Repertoire.Scripts.jquery-2.1.1.min.js")));
        }
    }
}
