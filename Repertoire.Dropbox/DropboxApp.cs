﻿using DropNet;

namespace Repertoire
{
    public class DropboxApp : IDropbox
    {
        public string ApiKey;
        public string AppSecret;
        public bool UseSandbox = true;

        public DropboxApp(string apiKey, string appSecret)
        {
            ApiKey = apiKey;
            AppSecret = appSecret;
        }

        public virtual DropNetClient CreateClient()
        {
            var client = new DropNetClient(ApiKey, AppSecret);
            client.UseSandbox = UseSandbox;
            return client;
        }
    }
}