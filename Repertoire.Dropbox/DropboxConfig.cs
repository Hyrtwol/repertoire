﻿using System;
using System.Configuration;

namespace Repertoire
{
    public static class DropboxConfig
    {
        public static readonly bool DropboxEnabled;
        public static readonly TimeSpan DropboxPullInterval;
        public static readonly string DropboxRootFolder;
        public static readonly string[] DropboxFileExtensions;

        public static readonly string DropboxApiKey;
        public static readonly string DropboxAppSecret;

        public static readonly string DropboxUserToken;
        public static readonly string DropboxUserSecret;

        static DropboxConfig()
        {
            var settings = ConfigurationManager.AppSettings;

            DropboxEnabled = settings.ToBool("DropboxEnabled");
            DropboxPullInterval = settings.ToTimeSpan("DropboxPullInterval");
            DropboxRootFolder = settings.ToString("DropboxRootFolder", "~/App_Data");

            var dropboxFileExtensions = settings.ToString("DropboxFileExtensions");
            DropboxFileExtensions = !string.IsNullOrEmpty(dropboxFileExtensions) ? dropboxFileExtensions.Split(';') : new string[0];

            DropboxApiKey = settings.ToString("DropboxApiKey");
            DropboxAppSecret = settings.ToString("DropboxAppSecret");

            DropboxUserToken = settings.ToString("DropboxUserToken");
            DropboxUserSecret = settings.ToString("DropboxUserSecret");
        }

        public static DropboxUser CreateDropboxUser()
        {
            var dropboxApp = new DropboxApp(DropboxApiKey, DropboxAppSecret);
            return new DropboxUser(dropboxApp, DropboxUserToken, DropboxUserSecret);
        }
    }
}