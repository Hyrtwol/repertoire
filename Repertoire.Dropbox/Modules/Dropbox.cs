﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using DropNet.Exceptions;

namespace Repertoire.Modules
{
    public class Dropbox : IHttpModule
    {
        private static readonly TimeSpan TTL = DropboxConfig.DropboxPullInterval;
        private static readonly DropboxUser User = DropboxConfig.CreateDropboxUser();
        private static readonly HashSet<string> DownloadExtensions = new HashSet<string>(DropboxConfig.DropboxFileExtensions);
        private static readonly string DataDirectory = AppDomain.CurrentDomain.GetData("DataDirectory").ToString();

        static volatile int _modCount = 0;
        private int _cnt;
        //private int _tick = 0;

        public void Init(HttpApplication httpApplication)
        {
            _cnt = ++_modCount;
            Debug.Print("[{0}]>>> {1}.Init()", _cnt, GetType().Name);
            httpApplication.BeginRequest += Application_BeginRequest;
            //httpApplication.EndRequest += Application_EndRequest;
        }

        public void Dispose()
        {
            Debug.Print("[{0}]>>> {1}.Dispose()", _cnt, GetType().Name);
        }

        private void Application_BeginRequest(object source, EventArgs e)
        {
            if(!DropboxConfig.DropboxEnabled) return;

            HttpApplication application = (HttpApplication)source;
            HttpContext context = application.Context;
            try
            {
                var request = context.Request;
                var httpMethod = request.HttpMethod.ToLower();
                if (httpMethod != "get") return;

                //var path = "~/App_Data" + request.CurrentExecutionFilePath;
                var path = DropboxConfig.DropboxRootFolder + request.CurrentExecutionFilePath;
                var appDataPath = context.Server.MapPath(path);
                DownloadFromDrombox(context, appDataPath);
            }
            catch (DropboxException ex)
            {
                var response = context.Response;
                var exr = ex.Response;

                if (exr.ErrorMessage.Contains("Could not create SSL/TLS secure channel.")) return;

                var sb = new StringBuilder();
                sb.AppendFormat("Status: {0}", ex.Response.StatusDescription);
                if (!string.IsNullOrWhiteSpace(exr.ErrorMessage))
                {
                    sb.Append("<br/>");
                    sb.Append(exr.ErrorMessage);
                }
                response.WriteErrorPage("Dropbox", sb.ToString(), ex.StackTrace);
                response.End();
            }
            catch (Exception ex)
            {
                var response = context.Response;
                response.WriteErrorPage("General", ex.Message, ex.StackTrace);
                response.End();
            }
        }

        //private void Application_EndRequest(object source, EventArgs e)
        //{
        //    HttpApplication application = (HttpApplication) source;
        //}

        private void DownloadFromDrombox(HttpContext context, string appDataPath)
        {
            var request = context.Request;
            string filePath = request.CurrentExecutionFilePath;
            string fileExtension = request.CurrentExecutionFilePathExtension;
            Debug.Print("[{0}]>>> '{1}' [{2}]", _cnt, filePath, fileExtension);
            if (string.IsNullOrEmpty(fileExtension) || !DownloadExtensions.Contains(fileExtension)) return;

            //string appDataPath = Path.Combine(DataDirectory, filePath);
            //string appDataPath = 
            Debug.Print("appDataPath='{0}'", appDataPath);

            //var localFile = new FileInfo(request.PhysicalPath);
            var localFile = new FileInfo(appDataPath);
            
            bool doDownload = true;
            if (localFile.Exists)
            {
                var age = DateTime.Now - localFile.LastWriteTime;
                doDownload = (age > TTL);
                Debug.Print("age={0} ttl={1} do={2}", age, TTL, doDownload);
            }

            var query = request.QueryString.ToString().ToLower();
            doDownload |= (query == "flush");

            if (!doDownload) return;

            var data = DownloadFromDropbox(request.Path);
            if (data != null) SaveFile(localFile, data);

            localFile = new FileInfo(appDataPath);
            if (localFile.Exists)
            {
                var dropboxLastWriteTime = localFile.LastWriteTime;
                Debug.Print("dropboxLastWriteTime={0}", dropboxLastWriteTime);
                context.Items["dropboxLastWriteTime"] = dropboxLastWriteTime;
            }
        }

        private static byte[] DownloadFromDropbox(string path)
        {
            Debug.Print("Downloading '{0}' from dropbox", path);

            var client = User.CreateClient();
            //return client.GetFile(path);
            try
            {
                return client.GetFile(path);
            }
            catch (DropboxException de)
            {
                if (de.Response.StatusCode == HttpStatusCode.NotFound) return null;
                throw;
            }
        }

        private static void SaveFile(FileInfo fileInfo, byte[] data)
        {
            if(fileInfo == null) throw new ArgumentNullException("fileInfo");
            if (data == null) throw new ArgumentNullException("data");

            Debug.Print("Saving {0} bytes to '{1}'", data.Length, fileInfo);
            var destDir = fileInfo.DirectoryName;
            if (destDir != null && !Directory.Exists(destDir)) Directory.CreateDirectory(destDir);
            //File.WriteAllBytes(fileInfo.FullName, data);
            using (var fs = fileInfo.Create())
            {
                fs.Write(data, 0, data.Length);
            }
        }
    }
}