using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Repertoire.Dropbox")]
[assembly: AssemblyDescription("HttpModule that enable a website to download files from Dropbox (deprecated due to api changes)")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Hyrtwol")]
[assembly: AssemblyProduct("Repertoire.Dropbox")]
[assembly: AssemblyCopyright("Copyright 2013 Thomas la Cour")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("0bc2fb29-7095-4d6c-b700-99bad2237751")]

[assembly: AssemblyVersion("1.2.0")]
[assembly: AssemblyFileVersion("1.2.0")]
