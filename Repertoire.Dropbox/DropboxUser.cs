﻿using System;
using System.Diagnostics;
using DropNet;
using DropNet.Models;

namespace Repertoire
{
    public class DropboxUser : IDropbox
    {
        private readonly DropboxApp _dropboxApp;
        private readonly UserLogin _userLogin;

        public DropboxUser(DropboxApp dropboxApp, string userToken, string userSecret)
        {
            _dropboxApp = dropboxApp;
            _userLogin = new UserLogin { Token = userToken, Secret = userSecret };
        }

        public string UserToken { get { return _userLogin.Token; } }
        public string UserSecret { get { return _userLogin.Secret; } }

        public virtual DropNetClient CreateClient()
        {
            //Debug.Print("Creating DropNetClient for {0}", _userLogin);
            var client = _dropboxApp.CreateClient();
            client.UserLogin = _userLogin;
            return client;
        }
    }
}