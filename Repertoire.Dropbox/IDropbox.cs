﻿using DropNet;

namespace Repertoire
{
    public interface IDropbox
    {
        DropNetClient CreateClient();
    }
}