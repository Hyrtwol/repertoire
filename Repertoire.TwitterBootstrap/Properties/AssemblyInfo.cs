using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Repertoire.TwitterBootstrap")]
[assembly: AssemblyDescription("Twitter Bootstrap as Embedded Resource")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Hyrtwol")]
[assembly: AssemblyProduct("Repertoire.TwitterBootstrap")]
[assembly: AssemblyCopyright("Copyright 2013 Thomas la Cour")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("8a9e7662-8f8a-40c9-8d0e-5e1270cdc664")]

[assembly: AssemblyVersion("1.2.0")]
[assembly: AssemblyFileVersion("1.2.0")]
