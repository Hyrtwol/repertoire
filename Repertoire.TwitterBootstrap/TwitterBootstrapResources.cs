﻿using System;
using System.Reflection;

namespace Repertoire
{
    public class TwitterBootstrapResources : IResourceModule
    {
        public static readonly Assembly ThisAssembly = typeof(TwitterBootstrapResources).Assembly;

        public void Load(IPageResourceRegistry registry)
        {
            registry.Register(
                new PageResource(
                    "bootstrap.min.js",
                    r => r.WriteJavaScriptReference("~/scripts/bootstrap.min.js"),
                    r => r.SetContentTypeByExtension(".js")
                          .SetResourceCacheHeaders()
                          .WriteResource(ThisAssembly, "Repertoire.Scripts.bootstrap.min.js")),

                new PageResource(
                    "bootstrap.min.css",
                    r => r.WriteJavaScriptReference("~/content/bootstrap.min.css"),
                    r => r.SetContentTypeByExtension(".css")
                          .SetResourceCacheHeaders()
                          .WriteResource(ThisAssembly, "Repertoire.Content.bootstrap.min.css")),
                
                new PageResource(
                    "glyphicons-halflings-regular.eot",
                    r => r.WriteJavaScriptReference("~/fonts/glyphicons-halflings-regular.eot"),
                    r => r.SetContentTypeByExtension(".eot")
                          .SetResourceCacheHeaders()
                          .WriteResource(ThisAssembly, "Repertoire.Fonts.glyphicons-halflings-regular.eot")),
                new PageResource(
                    "glyphicons-halflings-regular.svg",
                    r => r.WriteJavaScriptReference("~/fonts/glyphicons-halflings-regular.svg"),
                    r => r.SetContentTypeByExtension(".svg")
                          .SetResourceCacheHeaders()
                          .WriteResource(ThisAssembly, "Repertoire.Fonts.glyphicons-halflings-regular.svg")),
                new PageResource(
                    "glyphicons-halflings-regular.ttf",
                    r => r.WriteJavaScriptReference("~/fonts/glyphicons-halflings-regular.ttf"),
                    r => r.SetContentTypeByExtension(".ttf")
                          .SetResourceCacheHeaders()
                          .WriteResource(ThisAssembly, "Repertoire.Fonts.glyphicons-halflings-regular.ttf")),
                new PageResource(
                    "glyphicons-halflings-regular.woff",
                    r => r.WriteJavaScriptReference("~/fonts/glyphicons-halflings-regular.woff"),
                    r => r.SetContentTypeByExtension(".woff")
                          .SetResourceCacheHeaders()
                          .WriteResource(ThisAssembly, "Repertoire.Fonts.glyphicons-halflings-regular.woff"))

                );
        }
    }
}
