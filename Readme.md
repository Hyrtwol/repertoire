# Repertoire

Name                                                | [NuGet](https://www.nuget.org/packages?q=Repertoire) | [MyGet](https://www.myget.org/feed/Packages/artifacts)
--------------------------------------------------- | ---------------------------------------------------- | ------------------------------------------------------
[Repertoire][Repertoire-url]                        | [![][Repertoire-nuget]][Repertoire-nuget-url]              | [![][Repertoire-myget]][Repertoire-myget-url]
[Repertoire.Artwork][Artwork-url]                   | [![][Artwork-nuget]][Artwork-nuget-url]                    | [![][Artwork-myget]][Artwork-myget-url]
[Repertoire.Dropbox][Dropbox-url]                   | [![][Dropbox-nuget]][Dropbox-nuget-url]                    | [![][Dropbox-myget]][Dropbox-myget-url]
[Repertoire.jQuery][jQuery-url]                     | [![][jQuery-nuget]][jQuery-nuget-url]                      | [![][jQuery-myget]][jQuery-myget-url]
[Repertoire.Markdown][Markdown-url]                 | [![][Markdown-nuget]][Markdown-nuget-url]                  | [![][Markdown-myget]][Markdown-myget-url]
[Repertoire.TwitterBootstrap][TwitterBootstrap-url] | [![][TwitterBootstrap-nuget]][TwitterBootstrap-nuget-url]  | [![][TwitterBootstrap-myget]][TwitterBootstrap-myget-url]

[Repertoire-url]: https://bitbucket.org/Hyrtwol/Repertoire
[Repertoire-nuget]: https://img.shields.io/nuget/v/Repertoire.svg?maxAge=86400
[Repertoire-myget]: https://img.shields.io/myget/artifacts/v/Repertoire.svg?maxAge=86400&label=myget
[Repertoire-nuget-url]: https://www.nuget.org/packages/Repertoire
[Repertoire-myget-url]: https://www.myget.org/feed/artifacts/package/nuget/Repertoire
[Artwork-url]: https://bitbucket.org/Hyrtwol/Repertoire.artwork
[Artwork-nuget]: https://img.shields.io/nuget/v/Repertoire.Artwork.svg?maxAge=86400
[Artwork-myget]: https://img.shields.io/myget/artifacts/v/Repertoire.Artwork.svg?maxAge=86400&label=myget
[Artwork-nuget-url]: https://www.nuget.org/packages/Repertoire.Artwork
[Artwork-myget-url]: https://www.myget.org/feed/artifacts/package/nuget/Repertoire.Artwork
[Dropbox-url]: https://bitbucket.org/Hyrtwol/Repertoire.dropbox
[Dropbox-nuget]: https://img.shields.io/nuget/v/Repertoire.Dropbox.svg?maxAge=86400
[Dropbox-myget]: https://img.shields.io/myget/artifacts/v/Repertoire.Dropbox.svg?maxAge=86400&label=myget
[Dropbox-nuget-url]: https://www.nuget.org/packages/Repertoire.Dropbox
[Dropbox-myget-url]: https://www.myget.org/feed/artifacts/package/nuget/Repertoire.Dropbox
[jQuery-url]: https://bitbucket.org/Hyrtwol/Repertoire.jQuery
[jQuery-nuget]: https://img.shields.io/nuget/v/Repertoire.jQuery.svg?maxAge=86400
[jQuery-myget]: https://img.shields.io/myget/artifacts/v/Repertoire.jQuery.svg?maxAge=86400&label=myget
[jQuery-nuget-url]: https://www.nuget.org/packages/Repertoire.jQuery
[jQuery-myget-url]: https://www.myget.org/feed/artifacts/package/nuget/Repertoire.jQuery
[Markdown-url]: https://bitbucket.org/Hyrtwol/Repertoire.Markdown
[Markdown-nuget]: https://img.shields.io/nuget/v/Repertoire.Markdown.svg?maxAge=86400
[Markdown-myget]: https://img.shields.io/myget/artifacts/v/Repertoire.Markdown.svg?maxAge=86400&label=myget
[Markdown-nuget-url]: https://www.nuget.org/packages/Repertoire.Markdown
[Markdown-myget-url]: https://www.myget.org/feed/artifacts/package/nuget/Repertoire.Markdown
[TwitterBootstrap-url]: https://bitbucket.org/Hyrtwol/Repertoire.TwitterBootstrap
[TwitterBootstrap-nuget]: https://img.shields.io/nuget/v/Repertoire.TwitterBootstrap.svg?maxAge=86400
[TwitterBootstrap-myget]: https://img.shields.io/myget/artifacts/v/Repertoire.TwitterBootstrap.svg?maxAge=86400&label=myget
[TwitterBootstrap-nuget-url]: https://www.nuget.org/packages/Repertoire.TwitterBootstrap
[TwitterBootstrap-myget-url]: https://www.myget.org/feed/artifacts/package/nuget/Repertoire.TwitterBootstrap
